<?php

namespace Project\Application;

use \Nette\Mail\Message;

/**
 * IMailer
 */
interface IMailer
{
    /**
     * @param \Project\Application\ILogger $logger
     */
    public function setLogger(\Project\Application\ILogger $logger);

    /**
     * @param \Nette\Mail\Message $mail
     */
    public function send(Message $mail);
}
