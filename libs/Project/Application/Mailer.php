<?php

namespace Project\Application;

use Nette\Mail\SendmailMailer;
use Nette\Mail\Message;

/**
 * Mailer
 */
class Mailer extends SendmailMailer implements IMailer
{
    /** @var \Project\Application\ILogger */
    protected $logger = NULL;


    /**
     * @param \Project\Application\ILogger $logger
     */
    public function setLogger(\Project\Application\ILogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \Nette\Mail\Message $mail
     */
    public function send(Message $mail)
    {
        parent::send($mail);
        if ($this->logger) {
            $this->logger->mailLog($mail);
        }
    }
}
