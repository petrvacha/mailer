<?php

namespace Project\Application;

use Nette\Utils\Strings;

/**
 * Helpful utilities
 */
class Utilities extends \Nette\Object
{
    /**
     * @param string $hash
     * @param string $input
     * @param string $salt
     * @return bool
     */
    public static function check_sha1_hash($hash, $input, $salt = '')
    {
        return ($hash === sha1($input . $salt));
    }

    /**
     * @param string $input
     * @param string $salt
     * @param int|NULL $truncateTo
     * @return bool
     */
    public static function create_sha1_hash($input, $salt = '', $truncateTo = NULL)
    {
        return $truncateTo ? substr(sha1($input . $salt), 0, $truncateTo) : sha1($input . $salt);
    }

    /**
     * @param string $fullClassName
     * @return string
     */
    public static function convertClassNameToTableName($fullClassName)
    {
        return self::decamelize(lcfirst(substr($fullClassName, strrpos($fullClassName, '\\')+1)));
    }

    /**
     * @param string $className
     * @return string
     */
    public static function decamelize($className)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $className)), '_');
    }

    /**
     * @param string $fileName
     * @return string|FALSE
     */
    public static function getFileExtension($fileName)
    {
        return substr(strrchr($fileName, '.'), 1);
    }

    /**
     * @param string $date
     * @param bool $strict
     * @return string|FALSE
     */
    public static function verifyDate($date, $strict = TRUE)
    {
        $dateTime = DateTime::createFromFormat('m/d/Y', $date);
        if ($strict) {
            $errors = DateTime::getLastErrors();
            if (!empty($errors['warning_count'])) {
                return FALSE;
            }
        }
        return $dateTime !== FALSE;
    }

    /**
     * @param string $uri
     * @param string $path
     */
    public static function storeFile($uri, $path)
    {
        $ch = curl_init($uri);
        $fp = fopen($path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }
}
